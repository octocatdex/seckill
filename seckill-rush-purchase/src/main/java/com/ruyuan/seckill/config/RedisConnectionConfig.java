package com.ruyuan.seckill.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * Redis连接配置 对象
 */
@Configuration
@ConfigurationProperties(prefix = "spring.redis")
public class RedisConnectionConfig {


    /**
     * redis host ,like:127.0.0.1
     * 当configType 为manual（手工）时有效
     */
    private String host;

    /**
     * redis port ,like : 6379
     * 当configType 为manual（手工）时有效
     */
    private Integer port;

    /**
     * redis 密码
     * 当configType 为manual（手工）时有效
     */
    private String password;


    private Integer maxIdle;


    private Integer maxTotal;

    private Long maxWaitMillis;


    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getMaxIdle() {
        return maxIdle;
    }

    public void setMaxIdle(Integer maxIdle) {
        this.maxIdle = maxIdle;
    }

    public Integer getMaxTotal() {
        return maxTotal;
    }

    public void setMaxTotal(Integer maxTotal) {
        this.maxTotal = maxTotal;
    }

    public Long getMaxWaitMillis() {
        return maxWaitMillis;
    }

    public void setMaxWaitMillis(Long maxWaitMillis) {
        this.maxWaitMillis = maxWaitMillis;
    }

}
