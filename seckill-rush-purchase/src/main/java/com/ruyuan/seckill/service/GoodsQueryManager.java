package com.ruyuan.seckill.service;


import com.ruyuan.seckill.domain.vo.CacheGoods;

/**
 * 商品业务层
 *
 */
public interface GoodsQueryManager {

    /**
     * 缓存中查询商品的信息
     *
     * @param goodsId
     * @return
     */
    CacheGoods getFromCache(Integer goodsId);

}