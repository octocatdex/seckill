package com.ruyuan.seckill.domain;

import java.io.Serializable;

// @JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class MemberCoupon implements Serializable {

    private static final long serialVersionUID = 5545788652245350L;

    /**
     * 主键
     */
    private Integer mcId;

    /**
     * 优惠券表主键
     */
    private Integer couponId;

    /**
     * 会员表主键
     */
    private Integer memberId;

    /**
     * 使用时间
     */
    private Long usedTime;

    /**
     * 领取时间
     */
    private Long createTime;

    /**
     * 订单表主键
     */
    private Integer orderId;

    /**
     * 订单编号
     */
    private String orderSn;

    /**
     * 会员用户名
     */
    private String memberName;

    /**
     * 优惠券名称
     */
    private String title;

    /**
     * 优惠券面额
     */
    private Double couponPrice;

    /**
     * 优惠券门槛金额
     */
    private Double couponThresholdPrice;

    /**
     * 有效期--起始时间
     */
    private Long startTime;

    /**
     * 有效期--截止时间
     */
    private Long endTime;

    /**
     * 使用状态 0:未使用,1:已使用,2:已过期,3:已作废
     */
    private Integer usedStatus;

    /**
     * 商家ID
     */
    private Integer sellerId;

    /**
     * 商家名称
     */
    private String sellerName;

    /**
     * 使用状态文字（非数据库字段）
     */
    private String usedStatusText;

    /**
     * 使用范围 ALL:全品,CATEGORY:分类,SOME_GOODS:部分商品
     */
    private String useScope;

    /**
     * 范围关联的id
     * 全品或者商家优惠券时为0
     * 分类时为分类id
     * 部分商品时为商品ID集合
     */
    private String scopeId;


    public MemberCoupon() {
    }

    public MemberCoupon(CouponDO couponDO) {
        this.setCouponId(couponDO.getCouponId());
        this.setTitle(couponDO.getTitle());
        this.setStartTime(couponDO.getStartTime());
        this.setEndTime(couponDO.getEndTime());
        this.setCouponPrice(couponDO.getCouponPrice());
        this.setCouponThresholdPrice(couponDO.getCouponThresholdPrice());
        this.setSellerId(couponDO.getSellerId());
        this.setSellerName(couponDO.getSellerName());
        this.setUseScope(couponDO.getUseScope());
        this.setScopeId(couponDO.getScopeId());
    }

    public Integer getMcId() {
        return mcId;
    }

    public void setMcId(Integer mcId) {
        this.mcId = mcId;
    }

    public Integer getCouponId() {
        return couponId;
    }

    public void setCouponId(Integer couponId) {
        this.couponId = couponId;
    }

    public Integer getMemberId() {
        return memberId;
    }

    public void setMemberId(Integer memberId) {
        this.memberId = memberId;
    }

    public Long getUsedTime() {
        return usedTime;
    }

    public void setUsedTime(Long usedTime) {
        this.usedTime = usedTime;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public String getOrderSn() {
        return orderSn;
    }

    public void setOrderSn(String orderSn) {
        this.orderSn = orderSn;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Double getCouponPrice() {
        return couponPrice;
    }

    public void setCouponPrice(Double couponPrice) {
        this.couponPrice = couponPrice;
    }

    public Double getCouponThresholdPrice() {
        return couponThresholdPrice;
    }

    public void setCouponThresholdPrice(Double couponThresholdPrice) {
        this.couponThresholdPrice = couponThresholdPrice;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public Integer getUsedStatus() {
        return usedStatus;
    }

    public void setUsedStatus(Integer usedStatus) {
        this.usedStatus = usedStatus;
    }

    public String getUsedStatusText() {
        if (usedStatus == 0) {
            usedStatusText = "未使用";
        } else if (usedStatus == 2) {
            usedStatusText = "已过期";
        } else {
            usedStatusText = "已使用";
        }

        return usedStatusText;
    }

    public void setUsedStatusText(String usedStatusText) {
        this.usedStatusText = usedStatusText;
    }

    public Integer getSellerId() {
        return sellerId;
    }

    public void setSellerId(Integer sellerId) {
        this.sellerId = sellerId;
    }

    public String getSellerName() {
        return sellerName;
    }

    public void setSellerName(String sellerName) {
        this.sellerName = sellerName;
    }


    public String getUseScope() {
        return useScope;
    }

    public void setUseScope(String useScope) {
        this.useScope = useScope;
    }

    public String getScopeId() {
        return scopeId;
    }

    public void setScopeId(String scopeId) {
        this.scopeId = scopeId;
    }


}
