package com.ruyuan.seckill.domain.vo;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.ruyuan.seckill.domain.MemberAddress;

import java.io.Serializable;

/**
 * 收货人实体
 */
@SuppressWarnings("AlibabaPojoMustOverrideToString")
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class ConsigneeVO implements Serializable {

    private static final long serialVersionUID = 2499675140677613044L;
    private Integer consigneeId;
    /**
     * 收货人姓名
     */
    private String name;
    /**
     * 省
     */
    private String province;
    /**
     * 市
     */
    private String city;
    /**
     * 区
     */
    private String county;
    /**
     * 街道
     */
    private String town;
    /**
     * 详细地址
     */
    private String address;
    /**
     * 手机号
     */
    private String mobile;
    /**
     * 电话
     */
    private String telephone;
    /**
     * 省ID
     */
    private Integer provinceId;
    /**
     * 市ID
     */
    private Integer countyId;
    /**
     * 区ID
     */
    private Integer cityId;
    /**
     * 街道ID
     */
    private Integer townId;


    public ConsigneeVO() {
    }

    public ConsigneeVO(MemberAddress memberAddress) {

        this.setConsigneeId(memberAddress.getAddrId());
        this.setAddress(memberAddress.getAddr());

        this.setProvince(memberAddress.getProvince());
        this.setCity(memberAddress.getCity());
        this.setCounty(memberAddress.getCounty());
        this.setTown(memberAddress.getTown());

        this.setProvinceId(memberAddress.getProvinceId());
        this.setCityId(memberAddress.getCityId());
        this.setCountyId(memberAddress.getCountyId());
        if (memberAddress.getTownId() != null) {
            this.setTownId(memberAddress.getTownId());
        }
        this.setMobile(memberAddress.getMobile());
        this.setTelephone(memberAddress.getTel());
        this.setName(memberAddress.getName());

    }

    @Override
    public String toString() {
        return "ConsigneeVO{" +
                "consigneeId=" + consigneeId +
                ", name='" + name + '\'' +
                ", county='" + county + '\'' +
                ", province='" + province + '\'' +
                ", city='" + city + '\'' +
                ", town='" + town + '\'' +
                ", address='" + address + '\'' +
                ", mobile='" + mobile + '\'' +
                ", telephone='" + telephone + '\'' +
                ", countyId=" + countyId +
                ", provinceId=" + provinceId +
                ", cityId=" + cityId +
                ", townId=" + townId +
                '}';
    }

    public Integer getConsigneeId() {
        return consigneeId;
    }

    public void setConsigneeId(Integer consigneeId) {
        this.consigneeId = consigneeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public Integer getCountyId() {
        return countyId;
    }

    public void setCountyId(Integer countyId) {
        this.countyId = countyId;
    }

    public Integer getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(Integer provinceId) {
        this.provinceId = provinceId;
    }

    public Integer getCityId() {
        return cityId;
    }

    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }

    public Integer getTownId() {
        return townId;
    }

    public void setTownId(Integer townId) {
        this.townId = townId;
    }


}
